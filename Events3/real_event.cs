using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventsDelegates
{
    class Program
    {
        public class VideoEncoderEventArgs : EventArgs
        {
            public string VideoName { get; set; }
        }

        //public delegate void Func_Void_Arg_String(string s);

        static void SendEmailAfterEncoding(object sender, VideoEncoderEventArgs e)
        {
            Console.WriteLine($"Email: encoded by {sender}...");
            Console.WriteLine($"Email Header: Video Body: {e.VideoName} successfully encoded");
        }

        static void SendSmsAfterEncoding(object sender, VideoEncoderEventArgs e)
        {
            Console.WriteLine($"SMS: encoded by {sender}...");
            Console.WriteLine($"SMS: -- Body: {e.VideoName} successfully encoded --");
        }

        //private static Action<object, VideoEncoderEventArgs> invocationMethodsList;
        private static event EventHandler<VideoEncoderEventArgs> invocationMethodsList;

        private static void MpgVideoEncoding(string videoName)
        {
            Console.WriteLine($"Encoding video {videoName}");
            Thread.Sleep(3000);

            if (invocationMethodsList != null)
            {
                invocationMethodsList("Mpeg encoder", 
                    new VideoEncoderEventArgs { VideoName = videoName}); // fire all registered methods
            }

        }

        private static void Mp4VideoEncoding(string videoName)
        {
            Console.WriteLine($"Encoding video {videoName}");
            Thread.Sleep(3000);

            if (invocationMethodsList != null)
            {
                invocationMethodsList("Mp4 encoder",
                    new VideoEncoderEventArgs { VideoName = videoName});// fire all registered methods
            }

        }

        static void Main(string[] args)
        {
            // +=

            invocationMethodsList += SendEmailAfterEncoding;
            invocationMethodsList += SendSmsAfterEncoding;
            Mp4VideoEncoding("Batman movie...");

            invocationMethodsList -= SendEmailAfterEncoding;
            // this could only be done within the class
            // which holds the delegate as member
            invocationMethodsList = null;
            invocationMethodsList.Invoke(null, new VideoEncoderEventArgs { VideoName = "bla bla bla)" });
            MpgVideoEncoding("Superman movie...");

        }
    }
}
