using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventsDelegates
{
    class Program
    {

        //public delegate void Func_Void_Arg_String(string s);

        static void SendEmailAfterEncoding(string videoName)
        {
            Console.WriteLine($"Email Header: Video Body: {videoName} successfully encoded");
        }

        static void SendSmsAfterEncoding(string videoName)
        {
            Console.WriteLine($"SMS: -- Body: {videoName} successfully encoded --");
        }

        private static Action<string> invocationMethodsList;

        private static void VideoEncoding(string videoName)
        {
            Console.WriteLine($"Encoding video {videoName}");
            Thread.Sleep(3000);

            if (invocationMethodsList != null)
            {
                invocationMethodsList.Invoke(videoName); // fire all registered methods
            }

        }

        static void Main(string[] args)
        {
            // define delegate which is void and accepts string -- or just use Action
            // create function called SendEmailAfterEncoding (string videoName)
            //    in this function cw (email notification video name was successfully encoded!)
            // create function called SendSmsAfterEncoding (string videoName)
            //    in this function cw (SMS notification video name was successfully encoded!)
            // create delegate and insert both functions inside it as static member
            // create function EncodeVideo which recieves video name as argument
            //     Thread.Sleep(3000);
            //     now invoke the delegate!
            // from main call EncodeVideo
            // part 2:
            // now remove the sms using -=
            // now call EncodeVideo again

            // +=

            invocationMethodsList += SendEmailAfterEncoding;
            invocationMethodsList += SendSmsAfterEncoding;
            VideoEncoding("Batman movie...");

            invocationMethodsList -= SendEmailAfterEncoding;
            VideoEncoding("Superman movie...");

        }
    }
}
